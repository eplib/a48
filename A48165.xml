<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A48165">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A letter to a friend concerning the next Parliament's sitting at Oxford</title>
    <author>Philanglus.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A48165 of text R36352 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing L1644). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A48165</idno>
    <idno type="STC">Wing L1644</idno>
    <idno type="STC">ESTC R36352</idno>
    <idno type="EEBO-CITATION">15667284</idno>
    <idno type="OCLC">ocm 15667284</idno>
    <idno type="VID">104325</idno>
    <idno type="PROQUESTGOID">2240888496</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A48165)</note>
    <note>Transcribed from: (Early English Books Online ; image set 104325)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1152:24)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A letter to a friend concerning the next Parliament's sitting at Oxford</title>
      <author>Philanglus.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed for J.K.,</publisher>
      <pubPlace>[S.l.] :</pubPlace>
      <date>1681.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of original in the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament -- History.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
     <term>Great Britain -- Politics and government -- 1660-1688.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A letter to a friend concerning the next Parliament's sitting at Oxford.</ep:title>
    <ep:author>Philanglus</ep:author>
    <ep:publicationYear>1681</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>291</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-11</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-03</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-03</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A48165-t">
  <body xml:id="A48165-e0">
   <div type="letter" xml:id="A48165-e10">
    <pb facs="tcp:104325:1" rend="simple:additions" xml:id="A48165-001-a"/>
    <head xml:id="A48165-e20">
     <w lemma="a" pos="d" xml:id="A48165-001-a-0010">A</w>
     <w lemma="letter" pos="n1" rend="hi" xml:id="A48165-001-a-0020">LETTER</w>
     <w lemma="to" pos="acp" xml:id="A48165-001-a-0030">TO</w>
     <w lemma="a" pos="d" xml:id="A48165-001-a-0040">A</w>
     <w lemma="friend" pos="n1" rend="hi" xml:id="A48165-001-a-0050">FRIEND</w>
     <w lemma="concern" pos="vvg" xml:id="A48165-001-a-0060">Concerning</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-0070">the</w>
     <w lemma="next" pos="ord" xml:id="A48165-001-a-0080">Next</w>
     <w lemma="parliament" pos="ng1" xml:id="A48165-001-a-0090">PARLIAMENT'S</w>
     <w lemma="sit" pos="vvg" xml:id="A48165-001-a-0100">Sitting</w>
     <w lemma="at" pos="acp" xml:id="A48165-001-a-0110">at</w>
     <w lemma="0xf0rd" pos="crd" rend="hi" xml:id="A48165-001-a-0120">0XF0RD</w>
     <pc unit="sentence" xml:id="A48165-001-a-0130">.</pc>
    </head>
    <opener xml:id="A48165-e60">
     <salute xml:id="A48165-e70">
      <w lemma="honest" pos="j" xml:id="A48165-001-a-0140">Honest</w>
      <w lemma="Tom" pos="nn1" xml:id="A48165-001-a-0150">Tom</w>
      <pc xml:id="A48165-001-a-0160">,</pc>
     </salute>
    </opener>
    <p xml:id="A48165-e80">
     <w lemma="have" pos="vvg" xml:id="A48165-001-a-0170">HAving</w>
     <w lemma="undergo" pos="vvn" xml:id="A48165-001-a-0180">undergone</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-0190">the</w>
     <w lemma="gloomy" pos="j" xml:id="A48165-001-a-0200">Gloomy</w>
     <w lemma="day" pos="n1" xml:id="A48165-001-a-0210">Day</w>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-0220">and</w>
     <w lemma="news" pos="n1" reg="News" xml:id="A48165-001-a-0230">Newes</w>
     <w lemma="of" pos="acp" xml:id="A48165-001-a-0240">of</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-0250">the</w>
     <w lemma="parliament" pos="n2" rend="hi" xml:id="A48165-001-a-0260">Parliaments</w>
     <w lemma="dissolution" pos="n1" xml:id="A48165-001-a-0270">Dissolution</w>
     <pc xml:id="A48165-001-a-0280">,</pc>
     <w lemma="etc." pos="ab" reg="etc." xml:id="A48165-001-a-0290">&amp;c.</w>
     <w lemma="as" pos="acp" xml:id="A48165-001-a-0300">as</w>
     <w lemma="also" pos="av" xml:id="A48165-001-a-0310">also</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-0320">the</w>
     <w lemma="good" pos="j" xml:id="A48165-001-a-0330">good</w>
     <w lemma="news" pos="n1" reg="News" xml:id="A48165-001-a-0340">Newes</w>
     <w lemma="that" pos="cs" xml:id="A48165-001-a-0350">that</w>
     <w lemma="it" pos="pn" xml:id="A48165-001-a-0360">it</w>
     <w lemma="be" pos="vvz" xml:id="A48165-001-a-0370">is</w>
     <w lemma="his" pos="po" xml:id="A48165-001-a-0380">his</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A48165-001-a-0390">Majesties</w>
     <w lemma="pleasure" pos="n1" xml:id="A48165-001-a-0400">pleasure</w>
     <w lemma="to" pos="acp" xml:id="A48165-001-a-0410">to</w>
     <w lemma="order" pos="n1" xml:id="A48165-001-a-0420">Order</w>
     <w lemma="my" pos="po" xml:id="A48165-001-a-0430">my</w>
     <w lemma="lord" pos="n1" xml:id="A48165-001-a-0440">Lord</w>
     <w lemma="chancellor" pos="n1" xml:id="A48165-001-a-0450">Chancellor</w>
     <w lemma="to" pos="acp" xml:id="A48165-001-a-0460">to</w>
     <w lemma="issue" pos="n1" xml:id="A48165-001-a-0470">issue</w>
     <w lemma="out" pos="av" xml:id="A48165-001-a-0480">out</w>
     <pc unit="sentence" xml:id="A48165-001-a-0490">.</pc>
     <w lemma="writ" pos="n2" xml:id="A48165-001-a-0500">Writs</w>
     <w lemma="in" pos="acp" xml:id="A48165-001-a-0510">in</w>
     <w lemma="order" pos="n1" xml:id="A48165-001-a-0520">order</w>
     <w lemma="to" pos="acp" xml:id="A48165-001-a-0530">to</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-0540">the</w>
     <w lemma="election" pos="n1" xml:id="A48165-001-a-0550">Election</w>
     <w lemma="of" pos="acp" xml:id="A48165-001-a-0560">of</w>
     <w lemma="another" pos="d" xml:id="A48165-001-a-0570">another</w>
     <pc xml:id="A48165-001-a-0580">,</pc>
     <w lemma="all" pos="d" xml:id="A48165-001-a-0590">all</w>
     <w lemma="I" pos="pns" xml:id="A48165-001-a-0600">I</w>
     <w lemma="say" pos="vvb" xml:id="A48165-001-a-0610">say</w>
     <w lemma="by" pos="acp" xml:id="A48165-001-a-0620">by</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-0630">the</w>
     <w lemma="way" pos="n1" xml:id="A48165-001-a-0640">way</w>
     <w lemma="be" pos="vvz" xml:id="A48165-001-a-0650">is</w>
     <w lemma="this" pos="d" xml:id="A48165-001-a-0660">this</w>
     <pc xml:id="A48165-001-a-0670">,</pc>
     <pc join="right" xml:id="A48165-001-a-0680">(</pc>
     <foreign xml:id="A48165-e100" xml:lang="lat">
      <w lemma="n/a" pos="fla" xml:id="A48165-001-a-0690">Cavete</w>
      <w lemma="n/a" pos="fla" xml:id="A48165-001-a-0700">Angli</w>
     </foreign>
     <pc xml:id="A48165-001-a-0710">)</pc>
     <w lemma="look" pos="vvb" xml:id="A48165-001-a-0720">Look</w>
     <w lemma="to" pos="acp" xml:id="A48165-001-a-0730">to</w>
     <w lemma="it" pos="pn" xml:id="A48165-001-a-0740">it</w>
     <w lemma="you" pos="pn" xml:id="A48165-001-a-0750">you</w>
     <w lemma="freeholders" pos="ng1" reg="Freeholders'" xml:id="A48165-001-a-0760">Free-holders</w>
     <w lemma="of" pos="acp" xml:id="A48165-001-a-0770">of</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A48165-001-a-0780">England</w>
     <pc xml:id="A48165-001-a-0790">,</pc>
     <w lemma="that" pos="cs" xml:id="A48165-001-a-0800">that</w>
     <w lemma="you" pos="pn" xml:id="A48165-001-a-0810">you</w>
     <w lemma="observe" pos="vvb" xml:id="A48165-001-a-0820">observe</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-0830">the</w>
     <w lemma="good" pos="j" xml:id="A48165-001-a-0840">good</w>
     <w lemma="advice" pos="n1" xml:id="A48165-001-a-0850">Advice</w>
     <w lemma="contain" pos="vvn" xml:id="A48165-001-a-0860">contained</w>
     <w lemma="in" pos="acp" xml:id="A48165-001-a-0870">in</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-0880">the</w>
     <w lemma="writ" pos="n1" xml:id="A48165-001-a-0890">Writ</w>
     <pc xml:id="A48165-001-a-0900">:</pc>
     <w lemma="but" pos="acp" xml:id="A48165-001-a-0910">But</w>
     <w lemma="they" pos="pns" xml:id="A48165-001-a-0920">they</w>
     <w lemma="be" pos="vvb" xml:id="A48165-001-a-0930">are</w>
     <w lemma="to" pos="prt" xml:id="A48165-001-a-0940">to</w>
     <w lemma="sit" pos="vvi" reg="Sat" xml:id="A48165-001-a-0950">Sit</w>
     <w lemma="at" pos="acp" xml:id="A48165-001-a-0960">at</w>
     <w lemma="Oxford" pos="nn1" rend="hi" xml:id="A48165-001-a-0970">Oxford</w>
     <pc xml:id="A48165-001-a-0980">:</pc>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-0990">And</w>
     <w lemma="indeed" pos="av" xml:id="A48165-001-a-1000">indeed</w>
     <pc xml:id="A48165-001-a-1010">,</pc>
     <hi xml:id="A48165-e130">
      <w lemma="honest" pos="j" xml:id="A48165-001-a-1020">honest</w>
      <w lemma="Tom" pos="nn1" xml:id="A48165-001-a-1030">Tom</w>
     </hi>
     <pc rend="follows-hi" xml:id="A48165-001-a-1040">,</pc>
     <w lemma="I" pos="pns" xml:id="A48165-001-a-1050">I</w>
     <w lemma="think" pos="vvb" xml:id="A48165-001-a-1060">think</w>
     <w lemma="that" pos="cs" xml:id="A48165-001-a-1070">that</w>
     <w lemma="in" pos="acp" xml:id="A48165-001-a-1080">in</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-1090">the</w>
     <w lemma="time" pos="n1" xml:id="A48165-001-a-1100">time</w>
     <w lemma="of" pos="acp" xml:id="A48165-001-a-1110">of</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-1120">the</w>
     <w lemma="sickness" pos="n1" xml:id="A48165-001-a-1130">Sickness</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-1140">the</w>
     <w lemma="then" pos="av" xml:id="A48165-001-a-1150">then</w>
     <w lemma="parliament" pos="n1" rend="hi" xml:id="A48165-001-a-1160">Parliament</w>
     <w lemma="sit" pos="vvd" xml:id="A48165-001-a-1170">sat</w>
     <w lemma="at" pos="acp" xml:id="A48165-001-a-1180">at</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-1190">the</w>
     <w lemma="say" pos="j-vn" xml:id="A48165-001-a-1200">said</w>
     <w lemma="place" pos="n1" xml:id="A48165-001-a-1210">place</w>
     <pc unit="sentence" xml:id="A48165-001-a-1220">.</pc>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-1230">And</w>
     <w lemma="alas" pos="uh" xml:id="A48165-001-a-1240">alas</w>
     <pc xml:id="A48165-001-a-1250">!</pc>
     <w lemma="now" pos="av" xml:id="A48165-001-a-1260">now</w>
     <w lemma="Tom" pos="nn1" rend="hi" xml:id="A48165-001-a-1270">Tom</w>
     <w join="right" lemma="it" pos="pn" xml:id="A48165-001-a-1280">it</w>
     <w join="left" lemma="be" pos="vvz" xml:id="A48165-001-a-1281">'s</w>
     <w lemma="a" pos="d" xml:id="A48165-001-a-1290">a</w>
     <w lemma="sick" pos="j" xml:id="A48165-001-a-1300">sick</w>
     <w lemma="state" pos="n1" xml:id="A48165-001-a-1310">State</w>
     <pc xml:id="A48165-001-a-1320">,</pc>
     <w lemma="a" pos="d" xml:id="A48165-001-a-1330">a</w>
     <w lemma="sick" pos="j" xml:id="A48165-001-a-1340">sick</w>
     <w lemma="nation" pos="n1" xml:id="A48165-001-a-1350">Nation</w>
     <pc xml:id="A48165-001-a-1360">,</pc>
     <w lemma="a" pos="d" xml:id="A48165-001-a-1370">a</w>
     <w lemma="sick" pos="j" xml:id="A48165-001-a-1380">sick</w>
     <w lemma="people" pos="n1" xml:id="A48165-001-a-1390">People</w>
     <pc xml:id="A48165-001-a-1400">,</pc>
     <w lemma="all" pos="d" xml:id="A48165-001-a-1410">all</w>
     <w lemma="sick" pos="j" xml:id="A48165-001-a-1420">sick</w>
     <pc xml:id="A48165-001-a-1430">,</pc>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-1440">and</w>
     <w lemma="to" pos="prt" xml:id="A48165-001-a-1450">to</w>
     <w lemma="be" pos="vvi" xml:id="A48165-001-a-1460">be</w>
     <w lemma="fear" pos="vvn" xml:id="A48165-001-a-1470">feared</w>
     <w lemma="near" pos="acp" xml:id="A48165-001-a-1480">near</w>
     <w lemma="death" pos="n1" xml:id="A48165-001-a-1490">Death</w>
     <pc xml:id="A48165-001-a-1500">;</pc>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-1510">And</w>
     <w lemma="now" pos="av" xml:id="A48165-001-a-1520">now</w>
     <w lemma="to" pos="acp" xml:id="A48165-001-a-1530">to</w>
     <w lemma="Oxford" pos="nn1" rend="hi" xml:id="A48165-001-a-1540">Oxford</w>
     <w lemma="again" pos="av" xml:id="A48165-001-a-1550">again</w>
     <w lemma="must" pos="vmb" xml:id="A48165-001-a-1560">must</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-1570">the</w>
     <w lemma="parliament" pos="n1" rend="hi" xml:id="A48165-001-a-1580">parliament</w>
     <w lemma="go" pos="vvi" xml:id="A48165-001-a-1590">go</w>
     <pc unit="sentence" xml:id="A48165-001-a-1600">.</pc>
     <w lemma="but" pos="acp" xml:id="A48165-001-a-1610">But</w>
     <w lemma="Tom" pos="nn1" rend="hi" xml:id="A48165-001-a-1620">Tom</w>
     <w lemma="do" pos="vv2" xml:id="A48165-001-a-1630">dost</w>
     <w lemma="think" pos="vvi" xml:id="A48165-001-a-1640">think</w>
     <w lemma="London" pos="nng1" rend="hi" xml:id="A48165-001-a-1650">London's</w>
     <w lemma="air" pos="n1" xml:id="A48165-001-a-1660">Air</w>
     <w lemma="to" pos="prt" xml:id="A48165-001-a-1670">to</w>
     <w lemma="be" pos="vvi" xml:id="A48165-001-a-1680">be</w>
     <w lemma="infect" pos="vvn" xml:id="A48165-001-a-1690">infected</w>
     <pc unit="sentence" xml:id="A48165-001-a-1700">?</pc>
     <w lemma="I" pos="pns" xml:id="A48165-001-a-1710">I</w>
     <w lemma="must" pos="vmb" xml:id="A48165-001-a-1720">must</w>
     <w lemma="confess" pos="vvi" xml:id="A48165-001-a-1730">confess</w>
     <pc xml:id="A48165-001-a-1740">,</pc>
     <w lemma="a" pos="d" xml:id="A48165-001-a-1750">a</w>
     <w lemma="bold" pos="j" xml:id="A48165-001-a-1760">bold</w>
     <w lemma="rogue" pos="n1" xml:id="A48165-001-a-1770">Rogue</w>
     <w lemma="of" pos="acp" xml:id="A48165-001-a-1780">of</w>
     <w lemma="our" pos="po" xml:id="A48165-001-a-1790">our</w>
     <w lemma="intimacy" pos="n1" xml:id="A48165-001-a-1800">Intimacy</w>
     <w lemma="say" pos="vvd" xml:id="A48165-001-a-1810">said</w>
     <pc xml:id="A48165-001-a-1820">,</pc>
     <w lemma="if" pos="cs" xml:id="A48165-001-a-1830">If</w>
     <w lemma="it" pos="pn" xml:id="A48165-001-a-1840">it</w>
     <w lemma="be" pos="vvd" xml:id="A48165-001-a-1850">were</w>
     <pc xml:id="A48165-001-a-1860">,</pc>
     <w lemma="or" pos="cc" xml:id="A48165-001-a-1870">or</w>
     <w lemma="any" pos="d" xml:id="A48165-001-a-1880">any</w>
     <w lemma="part" pos="n2" xml:id="A48165-001-a-1890">Parts</w>
     <w lemma="adjoin" pos="vvg" reg="adjoining" xml:id="A48165-001-a-1900">adjoyning</w>
     <pc xml:id="A48165-001-a-1910">,</pc>
     <w lemma="it" pos="pn" xml:id="A48165-001-a-1920">it</w>
     <w lemma="be" pos="vvd" xml:id="A48165-001-a-1930">was</w>
     <w lemma="then" pos="av" xml:id="A48165-001-a-1940">then</w>
     <w lemma="the" pos="d" xml:id="A48165-001-a-1950">the</w>
     <w lemma="western" pos="j" xml:id="A48165-001-a-1960">Western</w>
     <w lemma="end" pos="n1" xml:id="A48165-001-a-1970">end</w>
     <pc unit="sentence" xml:id="A48165-001-a-1980">.</pc>
     <w lemma="but" pos="acp" xml:id="A48165-001-a-1990">But</w>
     <w lemma="however" pos="acp" xml:id="A48165-001-a-2000">however</w>
     <pc xml:id="A48165-001-a-2010">,</pc>
     <w lemma="we" pos="pns" xml:id="A48165-001-a-2020">we</w>
     <w lemma="know" pos="vvb" xml:id="A48165-001-a-2030">know</w>
     <w lemma="Oxford" pos="nn1" rend="hi" xml:id="A48165-001-a-2040">Oxford</w>
     <w lemma="be" pos="vvz" xml:id="A48165-001-a-2050">is</w>
     <w lemma="a" pos="d" xml:id="A48165-001-a-2060">a</w>
     <w lemma="clear" pos="j" xml:id="A48165-001-a-2070">clear</w>
     <w lemma="air" pos="n1" xml:id="A48165-001-a-2080">Air</w>
     <pc xml:id="A48165-001-a-2090">,</pc>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-2100">and</w>
     <w lemma="a" pos="d" xml:id="A48165-001-a-2110">a</w>
     <w lemma="goodly" pos="j" xml:id="A48165-001-a-2120">goodly</w>
     <w lemma="place" pos="n1" xml:id="A48165-001-a-2130">Place</w>
     <pc xml:id="A48165-001-a-2140">,</pc>
     <w lemma="likewise" pos="av" xml:id="A48165-001-a-2150">likewise</w>
     <w lemma="a" pos="d" xml:id="A48165-001-a-2160">a</w>
     <w lemma="sumptuous" pos="j" xml:id="A48165-001-a-2170">Sumptuous</w>
     <w lemma="theatre" pos="n1" rend="hi" xml:id="A48165-001-a-2180">Theatre</w>
     <w lemma="for" pos="acp" xml:id="A48165-001-a-2190">for</w>
     <w lemma="they" pos="pno" xml:id="A48165-001-a-2200">them</w>
     <w lemma="to" pos="acp" xml:id="A48165-001-a-2210">to</w>
     <w lemma="act" pos="n1" xml:id="A48165-001-a-2220">Act</w>
     <w lemma="their" pos="po" xml:id="A48165-001-a-2230">their</w>
     <w lemma="part" pos="n2" xml:id="A48165-001-a-2240">parts</w>
     <w lemma="in" pos="acp" xml:id="A48165-001-a-2250">in</w>
     <pc xml:id="A48165-001-a-2260">,</pc>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-2270">and</w>
     <w lemma="so" pos="av" xml:id="A48165-001-a-2280">so</w>
     <w lemma="let" pos="vvb" xml:id="A48165-001-a-2290">let</w>
     <w lemma="they" pos="pno" reg="'em" xml:id="A48165-001-a-2300">'um</w>
     <w lemma="march" pos="vvi" rend="hi" xml:id="A48165-001-a-2310">march</w>
     <w lemma="thitherwards" pos="av" xml:id="A48165-001-a-2320">thitherwards</w>
     <pc unit="sentence" xml:id="A48165-001-a-2330">.</pc>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-2340">And</w>
     <w lemma="I" pos="pns" xml:id="A48165-001-a-2350">I</w>
     <w lemma="with" pos="acp" xml:id="A48165-001-a-2360">with</w>
     <w lemma="they" pos="pno" xml:id="A48165-001-a-2370">them</w>
     <w lemma="all" pos="d" xml:id="A48165-001-a-2380">all</w>
     <w lemma="prosperity" pos="n1" xml:id="A48165-001-a-2390">Prosperity</w>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-2400">and</w>
     <w lemma="Felicity" pos="nn1" xml:id="A48165-001-a-2410">Felicity</w>
     <pc xml:id="A48165-001-a-2420">,</pc>
     <w lemma="and" pos="cc" xml:id="A48165-001-a-2430">and</w>
     <w lemma="withal" pos="av" reg="withal" xml:id="A48165-001-a-2440">withall</w>
     <w lemma="that" pos="cs" xml:id="A48165-001-a-2450">that</w>
     <w lemma="there" pos="av" xml:id="A48165-001-a-2460">there</w>
     <w lemma="may" pos="vmb" xml:id="A48165-001-a-2470">may</w>
     <w lemma="be" pos="vvi" xml:id="A48165-001-a-2480">be</w>
     <w lemma="no" pos="dx" xml:id="A48165-001-a-2490">no</w>
     <hi xml:id="A48165-e230">
      <w lemma="rogue" pos="n2" xml:id="A48165-001-a-2500">Rogues</w>
      <pc xml:id="A48165-001-a-2510">,</pc>
      <w lemma="pensioner" pos="n2" xml:id="A48165-001-a-2520">pensioners</w>
     </hi>
     <pc rend="follows-hi" xml:id="A48165-001-a-2530">,</pc>
     <w lemma="or" pos="cc" xml:id="A48165-001-a-2540">or</w>
     <w lemma="fellow" pos="n2" reg="Fellows" rend="hi" xml:id="A48165-001-a-2550">Fellowes</w>
     <w lemma="that" pos="cs" xml:id="A48165-001-a-2560">that</w>
     <w lemma="love" pos="vvb" xml:id="A48165-001-a-2570">love</w>
     <w lemma="their" pos="po" xml:id="A48165-001-a-2580">their</w>
     <w lemma="pocket" pos="n2" rend="hi" xml:id="A48165-001-a-2590">Pockets</w>
     <w lemma="better" pos="jc" xml:id="A48165-001-a-2600">better</w>
     <w lemma="than" pos="cs" xml:id="A48165-001-a-2610">than</w>
     <w lemma="their" pos="po" xml:id="A48165-001-a-2620">their</w>
     <w lemma="country" pos="n1" reg="Country" rend="hi" xml:id="A48165-001-a-2630">Countrey</w>
     <pc xml:id="A48165-001-a-2640">;</pc>
     <w lemma="which" pos="crq" xml:id="A48165-001-a-2650">which</w>
     <w lemma="I" pos="pns" xml:id="A48165-001-a-2660">I</w>
     <w lemma="cordial" pos="av-j" xml:id="A48165-001-a-2670">cordially</w>
     <w lemma="wish" pos="vvb" xml:id="A48165-001-a-2680">wish</w>
     <w lemma="for" pos="acp" xml:id="A48165-001-a-2690">for</w>
     <pc xml:id="A48165-001-a-2700">,</pc>
     <w lemma="who" pos="crq" xml:id="A48165-001-a-2710">who</w>
     <w lemma="be" pos="vvm" xml:id="A48165-001-a-2720">am</w>
     <pc xml:id="A48165-001-a-2730">,</pc>
    </p>
    <closer xml:id="A48165-e270">
     <salute xml:id="A48165-e280">
      <w lemma="honest" pos="j" xml:id="A48165-001-a-2740">Honest</w>
      <w lemma="Tom" pos="nn1" xml:id="A48165-001-a-2750">Tom</w>
      <pc xml:id="A48165-001-a-2760">,</pc>
     </salute>
     <signed xml:id="A48165-e290">
      <w lemma="thy" pos="po" xml:id="A48165-001-a-2770">Thy</w>
      <w lemma="old" pos="j" xml:id="A48165-001-a-2780">Old</w>
      <w lemma="friend" pos="n1" xml:id="A48165-001-a-2790">Friend</w>
      <w lemma="and" pos="cc" xml:id="A48165-001-a-2800">and</w>
      <w lemma="companion" pos="n1" xml:id="A48165-001-a-2810">Companion</w>
      <pc xml:id="A48165-001-a-2820">,</pc>
      <w lemma="n/a" pos="fla" xml:id="A48165-001-a-2830">Philanglus</w>
      <pc unit="sentence" xml:id="A48165-001-a-2840">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A48165-e300">
   <div type="colophon" xml:id="A48165-e310">
    <p xml:id="A48165-e320">
     <w lemma="print" pos="j-vn" xml:id="A48165-001-a-2850">Printed</w>
     <w lemma="for" pos="acp" xml:id="A48165-001-a-2860">for</w>
     <hi xml:id="A48165-e330">
      <w lemma="j." pos="ab" xml:id="A48165-001-a-2870">J.</w>
      <w lemma="k." pos="ab" xml:id="A48165-001-a-2880">K.</w>
     </hi>
     <w lemma="1681." pos="crd" xml:id="A48165-001-a-2890">1681.</w>
     <pc unit="sentence" xml:id="A48165-001-a-2900"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
